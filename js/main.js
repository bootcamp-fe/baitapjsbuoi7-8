// Create Array
var myArr = [] ;
var showArr = document.getElementById('array-display');

function addItemToArr(){
    
    var item = document.getElementById('iputItem').value*1;
    if(Number.isInteger(item)){
        myArr.push(item);
    } else {
        alert('Nhập vào số nguyên!')
    }
    showArr.innerHTML = "[" + myArr + "]";
}

function deleteArr(){
    myArr = [];
    showArr.innerHTML = myArr;
}

// Exercise 1

function ex1Handler(){
    var sum = 0;
    for(var i = 0; i < myArr.length;i++){
        if(myArr[i]>0){
            sum += myArr[i];
        }
    }
    document.querySelector('#ex1-result #ex-result-text').innerHTML = sum;
}

// Exercise 2

function ex2Handler(){
    var sum = 0;
    for(var i = 0; i < myArr.length; i++){
        if(myArr[i]>0&&myArr[i]!=0){
            sum++;
        }
    }
    document.querySelector('#ex2-result #ex-result-text').innerHTML = sum;
}

// Exercise 3

function ex3Handler(){
    var smallestNum = myArr[0];
    for(var i = 1 ; i < myArr.length; i++){
        if(myArr[i]<smallestNum){
            smallestNum = myArr[i];
        }
    }
    document.querySelector('#ex3-result #ex-result-text').innerHTML = smallestNum;
}

// Exercise 4

function ex4Handler(){
    var smallestPosNum ;
    var firstPosNum ;
    for (var i = 0 ; i < myArr.length ; i++){
        if(myArr[i]>0){
            firstPosNum = myArr[i];
            break;
        }
    }
    if(firstPosNum){
        smallestPosNum = firstPosNum;
        myArr.forEach((item) =>{
            if(item > 0 && item <firstPosNum){
                smallestPosNum = item;
            }
        })
        result = smallestPosNum;
    } else {
        result = "Mảng không chứa số dương!"
    }

    document.querySelector('#ex4-result #ex-result-text').innerHTML = result;
}


// Exercise 5

function ex5Handler(){
    var lastEvenNum;
    var hasEvenNum = false;
    var result;
    for( var i = 0 ; i < myArr.length; i++){
        if(myArr[i]%2==0){
            lastEvenNum = myArr[i];
            hasEvenNum = true;
        }
    }
    if(hasEvenNum){
        result = lastEvenNum;
    } else{
        result = "Không tìm thấy phần tử chẵn trong mảng!"
    }
    document.querySelector('#ex5-result #ex-result-text').innerHTML = result;
}

// Exercise 6

function ex6Handler(){
    var newArr = myArr.slice();
    var arrLength = myArr.length;
    var ex6Input1 = document.querySelector("#ex6-input #ex-input-1").value*1;
    var ex6Input2 = document.querySelector("#ex6-input #ex-input-2").value*1;
    var a = ex6Input1;
    var b = ex6Input2;
    var result;
    if(a < 0 || a > arrLength - 1 || b < 0 || b > arrLength - 1){
        result = "Index a/b không hợp lệ!"
    } else{
        newArr[a] = myArr[b];
        newArr[b] = myArr[a];
        result = "Chuỗi thu được : [" + newArr + "]";
    }
    document.querySelector('#ex6-result #ex-result-text').innerHTML = result;
}

// Exercise 7

function ex7Handler(){
    var inputArr = myArr.slice();
    var resultArr = [];
    for (var j = 0 ; j < myArr.length ; j++){
        var smallestNumIndex =0;
        var smallestNum = inputArr[0];
        for(var i = 0 ; i < inputArr.length ; i++){
            if(inputArr[i] < smallestNum){
                smallestNum = inputArr[i];
                smallestNumIndex = i;
            }
        }
        resultArr.push(smallestNum);
        inputArr.splice(smallestNumIndex,1);
    }
    
    var result = "["+ resultArr + "]";
    
    document.querySelector('#ex7-result #ex-result-text').innerHTML = result;
}


// Exercise 8
function isPrime(number){
    if(number<=1){
        return false;
    } else{
        for (var i = 2 ; i < number; i++){
            if(number%i==0){
                return false;
            }
        }
        return true;
    }
}

function ex8Handler(){
    var result = "Không tìm thấy số nguyên tố trong mảng!";
    for ( var i = 0 ; i < myArr.length ; i++ ){
        if(isPrime(myArr[i])){
            result = myArr[i];
            break;
        }
    }
    document.querySelector('#ex8-result #ex-result-text').innerHTML = result;
}

// Exercise 9
var ex9Arr = [];
var ex9ShowArr = document.getElementById('ex9-array-display');
function ex9addItem(){
    var ex9InputItem = document.getElementById('ex9IputItem').value*1;
    ex9Arr.push(ex9InputItem);
    ex9ShowArr.innerHTML = "[" + ex9Arr + "]";
}

function Ex9deleteArr(){
    ex9Arr =[];
    ex9ShowArr.innerHTML = ex9Arr;
}

function ex9Handler(){
    var result = 0;
    ex9Arr.forEach(
        (element) => {
            if(Number.isInteger(element)){
                result++;
            }
        }
    )
    document.querySelector('#ex9-result #ex-result-text').innerHTML = result;
}

// Exercise 10

function ex10Handler(){
    var result = "";
    var positiveNum = 0;
    var negativeNum =0;
    myArr.forEach((item) =>{
        if(item>0){
            positiveNum++;
        } else if (item < 0) {
            negativeNum++;
        }
    })
    if(myArr.length>0){
        if(positiveNum>negativeNum){
            result = "Số phần tử dương > Số phần tử âm ";
        } else if (positiveNum<negativeNum){
            result = "Số phần tử âm > Số phần tử dương";
        } else if (positiveNum!=0) {
            result = "Số phần tử dương = Số phần tử âm";
        } else {
            result = "Mảng không chứa số âm/số dương"
        }
    } else {
        result = "Mảng không chứa phần tử!";
    }
    
    document.querySelector('#ex10-result #ex-result-text').innerHTML = result;
}

